##################################################
# Name: DynamoDB Add Attribute
# Purpose: Adds an additional attribute to the DynamoDB table
# Author: Marcus Lau
####################################################

# Python Libaraies
import boto3
import argparse
from datetime import datetime
from botocore.exceptions import ClientError
import logging
import sys

# Global Variables
parser_values = None

# AWS Clients
dynamodb_client = boto3.client('dynamodb')

############# Helper Function #############
def getArg():
    '''
    Gets all arguments passed to the script
    '''
    parser = argparse.ArgumentParser(description='Missing required arguments')
    # Parse arguments
    parser.add_argument('-b', '--backup', dest='backup', default=False, action='store_true', help='Create DynamoDB table backup', required=False)

    require_args = parser.add_argument_group('required arguments')
    require_args.add_argument('-t', '--table', dest='table', help='Name of the DynamoDB table', required=True)
    require_args.add_argument('-pk', '--primary-key', dest='primary_key', help='Primary key of the DynamoDB table', required=True)
    require_args.add_argument('-c', '--column', dest='column', help='Name of the column on the DynamoDB table', required=True)
    require_args.add_argument('-v', '--value', dest='value', help='Value of the column on the DynamoDB table', required=True)
    global parser_values 
    parser_values = parser.parse_args()

def createBackup():
    '''
    Creates DynamoDB backup using the name format of YYYY-mm-dd-HH-MM-SS-TABLE_NAME
    '''
    backup_name = '{0}-{1}'.format(datetime.now().strftime("%Y-%m-%d-%H-%M-%S"), parser_values.table)
    logging.info('Creating backup for table {}'.format(parser_values.table))
    try:
        dynamodb_client.create_backup(
            TableName=parser_values.table,
            BackupName=backup_name
        )
        logging.info('Backup successfull')
    except ClientError as e:
        logging.error('Issue creating backup \n{}'.format(e))

def scanTable(exclusive_start_key=None):
    '''
    Performs scan on DynamoDB table
    @table: dynamodb table client
    @exclusive_start_key: the start key for a dynamodb scan

    Returns the response from a dynamodb scans
    '''
    scan_args = {
        'TableName' : parser_values.table,
        'FilterExpression': 'attribute_not_exists({})'.format(parser_values.column)
    }

    if exclusive_start_key != None:
        scan_args['ExclusiveStartKey'] = exclusive_start_key
    
    try:
        return dynamodb_client.scan(**scan_args)
    except ClientError as e:
        logging.error('Issue performing scan with LastEvaluatedKey: {0} \n{1}'.format(exclusive_start_key, e))

def updateItems( items):
    '''
    Updates Item on DynamoDB table
    @table: dynamodb table client
    @items: items to update from a dynamodb scan
    '''
    
    for item in items:
        print(item)
        try:
            dynamodb_client.update_item(
                TableName=parser_values.table,
                Key={parser_values.primary_key: item[parser_values.primary_key] },
                AttributeUpdates={
                    parser_values.column: {
                        'Value': {
                            'S': parser_values.value,
                        },
                    },
                },
            )
        except ClientError as e:
            logging.error('Issue updating item: {0} \n{1}'.format(item[parser_values.primary_key], e))

def performTableUpdate():
    '''
    Updates all items on the table without the specified column
    '''
    scan_response = scanTable()
    updateItems(scan_response['Items'])
    while 'LastEvaluatedKey' in scan_response:
        scan_response = scanTable(scan_response['Items'])
        updateItems(scan_response['Items'])

############# Main Function #############
def main():
    getArg()
    if parser_values.backup:
        createBackup()
    performTableUpdate()


if __name__ == "__main__":
    main()