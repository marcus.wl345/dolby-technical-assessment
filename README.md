# Dolby Technical Assessment
## Description
This project contains the answers to all the Dolby Technical Assessment.

## Required Tools (For script)
1. Python3 (+ Pip3)
2. AWS (DynamoDB setup + access to table)

## Script Execution 
**!NOTE** file paths for different OS
1. ensure your execution environment has aws credentials setup
2. run `pip install -r ./scripts/requirements.txt`
3. run `python ./scripts/dynamodb.py -t test -c test -v 1 -pk id` (This is the command specifically for this exercise)

## Script Command
```
usage: dynamodb.py [-h] [-b] -t TABLE -pk PRIMARY_KEY -c COLUMN -v VALUE

Missing required arguments

optional arguments:
  -h, --help            show this help message and exit
  -b, --backup          Create DynamoDB table backup

required arguments:
  -t TABLE, --table TABLE
                        Name of the DynamoDB table
  -pk PRIMARY_KEY, --primary-key PRIMARY_KEY
                        Primary key of the DynamoDB table
  -c COLUMN, --column COLUMN
                        Name of the column on the DynamoDB table
  -v VALUE, --value VALUE
                        Value of the column on the DynamoDB table
```

## Q&A
1. How you intend to perform the task. Be detailed, assume that someone else is going to actually execute your plan and will do “the wrong thing” if you don’t tell them otherwise.
    ```
    The following steps should be performed
    1. Determine how the updated data will be used by the different up/down stream services. This will determine whether we need to create any LSIs or GSIs
    2. Commmunicate changes out to relevant teams/stakeholders
    3. Determine the time that the update should be performed. This should be when there is the least impact to the systems. This information can be gathered by the statistics collected on user traffic and team communication to ensure updates will not affect downstream or upstream systems
    4. Write a script to perform the backup. Script must be written because of the number of items that need to be updated. (Logic of script is contained in the ./scripts/dyamodb.py)
        a. The script will perform an optional backup
        b. Scan the table for all items without the 'test' attribute (queries are not suitable for this use case)
        c. Update each item with the 'test' attribute
    5. Test the script on a dummy table or non-production environments
    6. Perform a backup of the database (this can be done in the script mentioned above, but since it is a onetime activity this action can also be done via the AWS GUI)
    7. Run the script in optimal time window
    8. Verify script success via a table scan with the filter of the 'test' attribute missing. Scan should not return any items
    ```
2. How you can be certain that all records have been updated?
    ```
    When you run the script it calls AWS APIs which tells us whether or not the update to an item was successful and unsuccessfully updates are outputed to stdout. At this point we should trust that all items that are not outputed to have successfully updated (AWS APIs are trustworthy). Otherwise if futher action is required you can scan the table with the filter of the 'test' attribute missing. The script is safe to rerun multiple times.
    ```
3. What else you should be careful about and how you would address those concerns?
    ```
    Concerns are addressed in the first question but to go more in detail:
    1. Make sure that the application is ready to handle the update to the database. This can be determined by team communication
    2. Determine optimal time window to perform update. This can be determined by team communication and looking at operation statistics
    3. Ensure script works via testing. Test can be done on the unit level as well as hitting a live non-production service
    4. Determine if new DynamoDB indices are required. This can be determined by team communication
    5. Determine impact on up/down stream services. This can be determined by team communication
    6. Ensure data integrity. This can be done from testing your script and ensuring that backups are avaliable in the event of any failures
    ```

4. If a developer/team made this request (and it wasn’t an interview question) for a service you’re jointly responsible for, is there anything you’d do differently?
    ```
    My approach would be the same. The main consideration is how this update would affect other teams and up/down stream services. Communication is critical!
    ```

## Feedback on assessment
1. Coding example should be shorter. Takes too much time if you are trying to make it repeatable.
2. Questions 3 and 4 are covered by question 1